export 'config/advertise.dart';
export 'config/general.dart';
export 'config/onboarding.dart';
export 'config/payments.dart';
export 'config/products.dart';
export 'config/smartchat.dart';

/// Server config demo for WooCommerce
/// Get more example for Opencart / Magento / Shopify from the example folder
const serverConfig = {
  "type": "woo",
  "url": "http://planettri.uk.tempcloudsite.com",

  /// document: https://docs.inspireui.com/fluxstore/woocommerce-setup/
  "consumerKey": "ck_50aac781297508a68b4faadeeccd170864ae0d82",
  "consumerSecret": "cs_42979443d59446435194e1c23d71f79a3e57ded9",

  /// Your website woocommerce. You can remove this line if it same url
  "blog": "http://planettri.uk.tempcloudsite.com",

  /// set blank to use as native screen
  "forgetPassword": "http://planettri.uk.tempcloudsite.com/wp-login.php?action=lostpassword"
};
